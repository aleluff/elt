const prices = {
	PC: 3.10,
	A: 1.08,
	B: 1.62,
	Attente: 27.95
}

function initAutocomplete() {
	const originInput = document.getElementById('origin-input');
	const destinationInput = document.getElementById('destination-input');

	const options = {
		types: ['geocode'],
		componentRestrictions: { country: "fr" }
	};

	new google.maps.places.Autocomplete(originInput, options);
	new google.maps.places.Autocomplete(destinationInput, options);
}

async function getDistance(origin, destination) {
	const service = new google.maps.DistanceMatrixService();
	return new Promise(resolve => {
		service.getDistanceMatrix(
			{
				origins: [origin],
				destinations: [destination],
				travelMode: 'DRIVING',
				unitSystem: google.maps.UnitSystem.METRIC,
				avoidHighways: false,
				avoidTolls: false
			}, (response, status) => {
				if (status !== 'OK') {
					console.error('Error: ' + status);
				}
				resolve(Math.floor(response.rows[0].elements[0].distance.value / 1000));
			});
	});
}

async function calculatePrice() {
	const origin = document.getElementById('origin-input').value;
	const destination = document.getElementById('destination-input').value;
	const majore = !document.getElementById('majore').checked;
	const attente = +document.getElementById('waitTime').value;
	const allerSimple = document.getElementById('allerSimple').checked;

	if (origin.length < 1 || destination.length < 1) {
		alert("Veuillez saisir une adresse de départ et d'arrivé");
		return;
	}

	let distance = await getDistance(origin, destination);
	let totalPrice = prices.PC + prices.Attente * attente;
	let cat = majore ? prices.A : prices.B;

	if (allerSimple) {
		totalPrice += distance * 2 * cat;
	} else {
		distance += await getDistance(destination, origin);
		totalPrice += distance * cat;
	}

	document.getElementById('result').innerHTML = 'Prix estimé: ' + totalPrice.toFixed(2) + ' €<br>Distance estimé: ' + distance + ' km<br>';
}

document.addEventListener('DOMContentLoaded', () => {

	document.getElementById("price_PC").textContent = prices.PC;
	document.getElementById("price_A").textContent = prices.A;
	document.getElementById("price_B").textContent = prices.B;
	document.getElementById("price_C").textContent = prices.A * 2;
	document.getElementById("price_D").textContent = prices.B * 2;
	document.getElementById("price_Attente").textContent = prices.Attente;

	initAutocomplete();
});
